using Godot;
using System;
using System.Collections.Generic;

public class TriggerLight : Node2D
{
    [Signal]
    public delegate void AddScore(int points);
    [Signal]
    public delegate void NotifyStage(int stage);

    [Export]
    public Color[] StageColors;
    [Export]
    public int CurrentStage = 0;
    [Export]
    public bool Loop = true;

    [Export]
    public int PointValue = 1000;
    [Export]
    public int StageValue = 500;


    private ColorRect _light;
    private Area2D _body;
    


    public override void _Ready()
    {
        _light = GetNode<ColorRect>("LightColor");
        _body = GetNode<Area2D>("ScoreArea");
    }

    public void OnAreaEntered(PhysicsBody2D body)
    {
        if(body.GetType() != typeof(Ball))
        {
            return;
        }
        int points = PointValue + (CurrentStage * StageValue);
        IncrementStage();

        EmitSignal(nameof(AddScore), points);
        EmitSignal(nameof(NotifyStage), CurrentStage);
        
    }

    public void IncrementStage()
    {
        CurrentStage++;
        if(CurrentStage >= StageColors.Length)
        {
            CurrentStage = 0;
        }
        UpdateStage();
    }

    public void SetStage(int stage)
    {
        if(stage > 0 && CurrentStage <= StageColors.Length)
        {
            CurrentStage = stage;
        }
        else
        {
            CurrentStage = Mathf.Clamp(stage, 0, StageColors.Length - 1);
        }
        UpdateStage();
    }

    private void UpdateStage()
    {
        _light.Color = StageColors[CurrentStage];
    }

}
