using Godot;
using System;

public class Retry_Screen : Control
{
    [Signal]
    public delegate void OnRetry();

    private Label _scoreText;

    public override void _Ready()
    {
        _scoreText = GetNode<Label>("Score");
    }

    public void SetVisible()
    {
        var scoreBoard = GetNode<Score>("/root/Main/Score Board");
        if(scoreBoard != null)
        { 
            _scoreText.Text = scoreBoard.GetScoreText();
        }
        this.Visible = true;
        
    }

    public void SetInvisible()
    {
        this.Visible = false;
    }

    public void OnRetryPressed()
    {
        EmitSignal(nameof(OnRetry));
    }
}
