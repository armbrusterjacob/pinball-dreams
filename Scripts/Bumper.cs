using Godot;
using System;

public class Bumper : RigidBody2D
{
    [Signal]
    public delegate void AddScore(int points);

    public int PointValue= 1;


    public void OnBumperBodyEntered(Node node)
    {
        EmitSignal(nameof(AddScore), PointValue);
    }
}
