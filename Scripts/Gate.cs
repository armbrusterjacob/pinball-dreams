using Godot;
using System;

public class Gate : Node2D
{
    [Export]
    public Boolean Active = true;

    private AnimatedSprite _sprite;
    private CollisionShape2D _collision;

    public override void _Ready()
    {
        _sprite = GetNode<AnimatedSprite>("Gate Body/AnimatedSprite");
        _collision = GetNode<CollisionShape2D>("Gate Body/CollisionShape2D");

        if(Active)
        {
            _collision.Disabled = false;
            _sprite.SetAnimation("closed");
        }else
        {
            _sprite.SetAnimation("open");
            _collision.Disabled = true;
        }
    }

    public void CloseGate(PhysicsBody2D body)
    {
        _collision.Disabled = false;
        _sprite.SetAnimation("closed");
    }

    public void OpenGate()
    {
        _sprite.SetAnimation("open");
        _collision.Disabled = true;
    }

    public void Toggle()
    {
        Active = !Active;
        if(Active)
        {
            // CloseGate();
        }else
        {
            OpenGate();
        }
    }
}
