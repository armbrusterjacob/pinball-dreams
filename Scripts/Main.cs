using Godot;
using System;
using System.Linq;

public class Main : Node2D
{
    [Export]
    public Ball _mainBall;
    [Export]
    public int InitialBalls = 3;

    [Export]
    public int FlipperScore = 100;
    [Export]
    public int BumperScore = 1000;
    [Export]
    public int LightScore = 2000;
    [Export]
    public int LightStageScore = 500;


    public int Score = 0;
    public int Balls = 3;
    public Flipper LastTouchedFlipper = null;

    private Position2D _spawnPoint;
    private Score _scoreBoard;
    private Retry_Screen _retryScreen;
    private Gate _springGate;


    public override void _Ready()
    {
        Balls = InitialBalls;
        _mainBall = GetNode<Ball>("Main Ball");
        _spawnPoint = GetNode<Position2D>("Spawn Point");
        _scoreBoard = GetNode<Score>("Score Board");
        _retryScreen = GetNode<Retry_Screen>("Retry Screen");
        _springGate = GetNode<Gate>("Obstacles/Spring Gate");
        _mainBall.TeleportTo(_spawnPoint);
        AddSignals();

        _scoreBoard.UpdateScore(Score);
        _scoreBoard.UpdateBalls(Balls);
    }

    public void AddSignals()
    {
        // Signals for score related obstacles
        foreach(Node child in GetNode<Node2D>("Obstacles").GetChildren())
        {
            if(child.GetType() == typeof(Bumper))
            {
                child.Connect("AddScore", this, "ScoreTrigger");
                Bumper b = (Bumper) child;
                b.PointValue = BumperScore;
            }
            if(child.GetType() == typeof(Flipper))
            {
                child.Connect("AddScore", this, "ScoreTrigger");
                Flipper b = (Flipper) child;
                b.PointValue = FlipperScore;
            }
            if(child.GetType() == typeof(TriggerLight))
            {
                child.Connect("AddScore", this, "ScoreTrigger");
                TriggerLight b = (TriggerLight) child;
                b.PointValue = LightScore;
                b.StageValue = LightStageScore;
            }
        }
    }

    public void AddPoints(int points)
    {
        Score += points;
        _scoreBoard.UpdateScore(Score);
    }

    public void ScoreTrigger(int points)
    {
        AddPoints(points);

    }

    public void OnLoss()
    {
        Balls--;
        if(!CheckGameover())
        {
            _mainBall.TeleportTo(_spawnPoint);
        }
        _springGate.OpenGate();
        _scoreBoard.UpdateBalls(Balls);
    }

    public bool CheckGameover()
    {
        if(Balls < 0){
            GameOver();
            return true;
        }
        return false;
    }

    public void GameOver()
    {
        Balls = 0;
        _retryScreen.SetVisible();
    }

    public void Restart()
    {
        GetTree().ReloadCurrentScene();
    }
}
