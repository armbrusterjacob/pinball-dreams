using Godot;
using System;

public class Launcher : Node2D
{
    private Vector2 _initialTopPoint;
    private Vector2 _initialBottomPoint;
    private Vector2 _compressionPoint;
    private KinematicBody2D _springTop;
    private Sprite _springBottom;

    // time in seconds it takes to fully compress the launcher
    [Export]
    public float CompressionTime = 1f;
    [Export]
    public float LaunchTime = 0.25f;
    [Export]
    public float LaunchForce = 100f;
    [Export]
    public float MinimumLaunchCompression = 0.1f;

    public float LastCompression = 0f;
    public float CompressionPercent = 0f;

    // The smallest scale value the _springBottom can shrink to
    [Export]
    public float LocalScaleMinimum = .2f;
    private float _localInitialScale;

    public override void _Ready()
    {
        _compressionPoint = GetNode<Position2D>("Compression Point").Position;
        _springTop = GetNode<KinematicBody2D>("Spring Top");
        _springBottom = GetNode<Sprite>("Spring Bottom");
        _initialTopPoint = _springTop.Position;
        _initialBottomPoint = _springBottom.Position;

        _localInitialScale = _springBottom.Scale.y;
    }

    public override void _PhysicsProcess(float delta)
    {
        if(Input.IsActionPressed("compress_launcher"))
        {
            CompressionPercent += delta / CompressionTime;
            LastCompression = Mathf.Clamp(CompressionPercent, 0, 1); 
        }
        else
        {
            float initialCompression = CompressionPercent;
            // if the transition between higher then lower than the minimium launch compression is made
            CompressionPercent -= delta / LaunchTime;
            // if(initialCompression > MinimumLaunchCompression && CompressionPercent < MinimumLaunchCompression)
            // {
                
            // }
        }
        CompressionPercent = Mathf.Clamp(CompressionPercent, 0, 1);
        
        float newTopY = Mathf.Lerp(_initialTopPoint.y, _compressionPoint.y * 2, CompressionPercent);
        _springTop.SetPosition(new Vector2(_springTop.Position.x, newTopY));
        
        float newBottomY = Mathf.Lerp(_initialBottomPoint.y, _compressionPoint.y * 2, CompressionPercent);
        _springBottom.SetPosition(new Vector2(_springBottom.Position.x, newBottomY));

        float newYScale = Mathf.Lerp(_localInitialScale, LocalScaleMinimum / _localInitialScale, CompressionPercent);
        _springBottom .SetScale(new Vector2(_springBottom.Scale.x, newYScale));

        

    }
}
