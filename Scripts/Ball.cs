using Godot;
using System;

public class Ball : RigidBody2D
{
    [Export] 
    public float MaxSpeed = 100f;
    [Export]
    public float TeleportBuffer = 0.5f;

    public Position2D TeleportLocation;
    public Boolean Teleporting { get; set; } = false;
    public float TeleportTimeRemaining;

    public override void _PhysicsProcess(float delta)
    {
        SetLinearVelocity(LinearVelocity.Clamped(MaxSpeed));
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {
        if(Teleporting)
        {
            SetLinearVelocity(Vector2.Zero);
            SetAngularVelocity(0);
            state.Transform = TeleportLocation.Transform;

            Teleporting = false;
        }
    }

    public void TeleportTo(Position2D pos)
    {
        TeleportLocation = pos;
        Teleporting = true;
    }
}
