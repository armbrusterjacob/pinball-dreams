using Godot;
using System;

public class LossArea : Area2D
{
    [Signal]
    public delegate void Loss();

    public void OnLossBodyEntered(PhysicsBody2D body)
    {   
        if(body.GetType() == typeof(Ball))
        {
            EmitSignal(nameof(Loss));
        }
    }

}
