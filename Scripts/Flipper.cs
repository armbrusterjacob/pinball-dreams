using Godot;
using System;
 
public class Flipper : KinematicBody2D
{
    [Signal]
    public delegate void AddScore();

    [Export]
    public float RangeDegrees = 90f;
    [Export]
    public float FlipSpeed = 100f;
    [Export] 
    float RetractSpeed = 100f;
    [Export]
    public String ControlString = "flip_left";
    [Export] 
    public Boolean Active = true;

    [Export]
    public int PointValue = 2;

    private float _startDegrees;
    private float _completion = 0;


    public override void _Ready()
    {
        // find all flippers

        _startDegrees = Mathf.Rad2Deg(Rotation);
        // Set rotation to default
    }

    public override void _PhysicsProcess(float delta)
    {
        LerpRotate(delta);

    }

    public void LerpRotate(float delta)
    {
        if (Active && Input.IsActionPressed(ControlString))
        {
            _completion += FlipSpeed * delta;
        }else
        {
            _completion -= RetractSpeed * delta;
        }
        _completion = Mathf.Clamp(_completion, 0, 1);
        Rotation = (Mathf.Lerp(Mathf.Deg2Rad(_startDegrees), Mathf.Deg2Rad(_startDegrees - RangeDegrees), _completion));
    }

    public void LinearRotate()
    {
        if (Active && Input.IsActionPressed(ControlString))
        {
            Rotate(Mathf.Deg2Rad(-FlipSpeed));
        }else
        {
            Rotate(Mathf.Deg2Rad(RetractSpeed));
        }
        Rotation = Mathf.Clamp(Rotation, Mathf.Deg2Rad(-RangeDegrees), Mathf.Deg2Rad(_startDegrees));
    }

    public void Activate()
    {
        Active = true;
    }
    public void Deactivate()
    {
        Active = false;
    }

    public void OnScoreAreaEntered(PhysicsBody2D body)
    {
        EmitSignal(nameof(AddScore), PointValue);
    }
}
