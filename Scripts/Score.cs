using Godot;
using System;

public class Score : Control
{
    
    [Export]
    public int MaxDigits = 8;

    private Label _scoreLabel;
    private Label _ballsLabel;

    public override void _Ready()
    {
        _scoreLabel = GetNode<Label>("Score");
        _ballsLabel = GetNode<Label>("Balls Remaining/Count");
    }

    public void UpdateScore(int score)
    {
        String scoreString = score.ToString();
        if(scoreString.Length < MaxDigits)
        {
            scoreString = new String('0', MaxDigits - scoreString.Length) + scoreString;
        }
        _scoreLabel.Text = scoreString;
    }

    public void UpdateBalls(int balls)
    {
        _ballsLabel.Text = balls.ToString();
    }

    public String GetScoreText()
    {
        return _scoreLabel.Text;
    }
}
